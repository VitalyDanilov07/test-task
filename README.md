Тестовое задание 

1. git clone git@bitbucket.org:VitalyDanilov07/test-task.git
2. cd test-task
3. composer install

В базах есть тестовые данные, готовые к синхронизации.
Во всех базах в полях с текстовыми значениями, для удоства стоит препикс базы.
db1 employee9 - имя сотрудника 9 из БД1
Остатки по товарам из БД1 - 1001 
Остатки по товарам из БД2 - 2001
Базы в корне проекта -  
/systech01.db
/systech02.db

Консольная сомманда для запуска сихронизации баз:
  php bin/console synchronization:database
