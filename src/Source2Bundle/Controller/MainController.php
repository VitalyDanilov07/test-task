<?php

namespace Source2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MainController extends Controller
{
    public function indexAction()
    {
        return $this->render('Source2Bundle:Default:index.html.twig');
    }
}
