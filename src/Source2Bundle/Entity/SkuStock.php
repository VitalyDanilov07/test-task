<?php

namespace Source2Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * SkuStock
 *
 * @ORM\Table(name="sku_stock")
 * @ORM\Entity(repositoryClass="Source2Bundle\Repository\SkuStockRepository")
 */
class SkuStock
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=true)
     */
    private $modifiedAt;


    /**
     * @var int
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @OneToOne(targetEntity="Source2Bundle\Entity\Sku", cascade={"persist", "remove"} )
     * @ORM\JoinColumn(name="sku_id", referencedColumnName="id", nullable=false)
     */
    private $sku;

    /**
     * Set sku
     *
     * @param Sku $sku
     *
     * @return SkuStock
     */
    public function setSku(Sku $sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * @return  Sku
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * SkuStock constructor.
     * @param $sku Sku
     * @param $stock integer
     * @throws \Exception
     */
    public function __construct($sku, $stock)
    {
        $this->setSku($sku);
        $this->setStock($stock);
        $this->setCreatedAt(new \DateTime('now'));
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return SkuStock
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return SkuStock
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return SkuStock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }
}

