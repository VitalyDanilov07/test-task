<?php

namespace AppBundle\Command;

use AppBundle\Entity\Employee as Employee1;
use AppBundle\Entity\Outlet as Outlet1;
use AppBundle\Entity\Owner as Owner1;
use AppBundle\Entity\Sku as Sku1;
use Source2Bundle\Entity\Employee as Employee2;
use Source2Bundle\Entity\Outlet as Outlet2;
use Source2Bundle\Entity\Sku as Sku2;
use Source2Bundle\Entity\SkuStock as SkuStock2;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SynchronizationDatabaseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('synchronization:database')
            ->setDescription('Синхронизация данных между двумя разнородными источниками.')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Synchronization started:');
        $this->syncEmployee();
        $output->writeln('Synchronization Database Employee complete.');
        $this->syncOutlet();
        $output->writeln('Synchronization Database Outlet complete.');
        $this->syncSku();
        $output->writeln('Synchronization Database Sku complete.');
    }

    public function syncOutlet(){
        $em1 = $this->getContainer()->get('doctrine')->getManager('default');
        $em2 = $this->getContainer()->get('doctrine')->getManager('source2');
        $outletRepoDb1 = $em1->getRepository('AppBundle:Outlet');
        $outletRepoDb2 = $em2->getRepository('Source2Bundle:Outlet');

        foreach ($outletRepoDb1->findAll() as $outletDb1){
            $existOutlet2 = $outletRepoDb2->findOneBy(['createdAt' => $outletDb1->getCreatedAt()]);
            if ($existOutlet2){
                if ($existOutlet2->getModifiedAt() < $outletDb1->getModifiedAt()){
                    $existOutlet2->setName($outletDb1->getName());
                    $existOutlet2->setOwnerName($outletDb1->getOwner()->getName());
                    $existOutlet2->setModifiedAt($outletDb1->getModifiedAt());
                    $em2->persist($existOutlet2);
                    $em2->flush();
                }
            }else{
                $newOutle2 = new Outlet2($outletDb1->getName(), $outletDb1->getOwner()->getName());
                $newOutle2->setCreatedAt($outletDb1->getCreatedAt());
                $newOutle2->setModifiedAt($outletDb1->getModifiedAt());
                $em2->persist($newOutle2);
                $em2->flush();
            }
        }

        foreach ($outletRepoDb2->findAll() as $outletDb2){
            $existOutlet1 = $outletRepoDb1->findOneBy(['createdAt' => $outletDb2->getCreatedAt()]);
            if ($existOutlet1){
                if ($existOutlet1->getModifiedAt() < $outletDb2->getModifiedAt()){
                    $existOutlet1->setName($outletDb2->getName());
                    $existOutlet1->getOwner()->setName($outletDb2->getOwnerName());
                    $existOutlet1->setModifiedAt($outletDb2->getModifiedAt());
                    $em1->persist($existOutlet1);
                    $em1->flush();
                }
            }else{
                $newOwner1 = new Owner1($outletDb2->getOwnerName());
                $newOwner1->setCreatedAt($outletDb2->getCreatedAt());
                $newOwner1->setModifiedAt($outletDb2->getModifiedAt());
                $em1->persist($newOwner1);
                $em1->flush();
                $newOutlet1 = new Outlet1($outletDb2->getName(), $newOwner1);
                $newOutlet1->setCreatedAt($outletDb2->getCreatedAt());
                $newOutlet1->setModifiedAt($outletDb2->getModifiedAt());
                $em1->persist($newOutlet1);
                $em1->flush();
            }
        }
    }

    public function syncSku(){
        $em1 = $this->getContainer()->get('doctrine')->getManager('default');
        $em2 = $this->getContainer()->get('doctrine')->getManager('source2');
        $skuRepoDb1 = $em1->getRepository('AppBundle:Sku');
        $skuStockRepoDb2 = $em2->getRepository('Source2Bundle:SkuStock');

        foreach ($skuStockRepoDb2->findAll() as $skuStockDb2){
            $existSkuDb1 =  $skuRepoDb1->findOneBy(['createdAt' => $skuStockDb2->getCreatedAt()]);
            if ($existSkuDb1){
                if ($existSkuDb1->getModifiedAt() < $skuStockDb2->getModifiedAt()){
                    $existSkuDb1->setName($skuStockDb2->getSku()->getName());
                    $existSkuDb1->setStock($skuStockDb2->getStock());
                    $existSkuDb1->setModifiedAt($skuStockDb2->getModifiedAt());
                    $em1->persist($existSkuDb1);
                    $em1->flush();
                }
            }else{
                $newSkuDb1 = new Sku1($skuStockDb2->getSku()->getName(), $skuStockDb2->getStock());
                $newSkuDb1->setCreatedAt($skuStockDb2->getCreatedAt());
                $newSkuDb1->setModifiedAt($skuStockDb2->getModifiedAt());
                $em1->persist($newSkuDb1);
                $em1->flush();
            }
        }

        foreach ($skuRepoDb1->findAll() as $skuDb1){
            $existSkuStockDb2 = $skuStockRepoDb2->findOneBy(['createdAt' => $skuDb1->getCreatedAt()]);
            if ($existSkuStockDb2){
                if ($existSkuStockDb2->getModifiedAt() < $skuDb1->getModifiedAt()){
                    $existSkuStockDb2->getSku()->setName($skuDb1->getName());
                    $existSkuStockDb2->setStock($skuDb1->getStock());
                    $existSkuStockDb2->setModifiedAt($skuDb1->getModifiedAt());
                    $existSkuStockDb2->getSku()->setModifiedAt($skuDb1->getModifiedAt());
                    $em2->persist($existSkuStockDb2);
                    $em2->flush();
                }
            }else{
                $newSkuDb2 = new Sku2($skuDb1->getName());
                $newSkuDb2->setCreatedAt($skuDb1->getCreatedAt());
                $newSkuDb2->setModifiedAt($skuDb1->getModifiedAt());
                $em2->persist($newSkuDb2);
                $em2->flush();
                $newSkuStock2 = new SkuStock2($newSkuDb2, $skuDb1->getStock());
                $newSkuStock2->setCreatedAt($skuDb1->getCreatedAt());
                $newSkuStock2->setModifiedAt($skuDb1->getModifiedAt());
                $em2->persist($newSkuStock2);
                $em2->flush();
            }
        }
    }

    public function syncEmployee(){
        $em1 = $this->getContainer()->get('doctrine')->getManager('default');
        $em2 = $this->getContainer()->get('doctrine')->getManager('source2');
        $employeeRepoDb1 = $em1->getRepository('AppBundle:Employee');
        $employeeRepoDb2 = $em2->getRepository('Source2Bundle:Employee');

        foreach ($employeeRepoDb2->findAll() as $employee2){
            $existEmployee1 = $employeeRepoDb1->findOneBy(['createdAt' => $employee2->getCreatedAt()]);
            if($existEmployee1){
                if ($existEmployee1->getModifiedAt() < $employee2->getModifiedAt()){
                    $existEmployee1->setName($employee2->getName());
                    $existEmployee1->setModifiedAt($employee2->getModifiedAt());
                    $em1->persist($existEmployee1);
                }
            }else{
                $newEmployee1 = new Employee1($employee2->getName());
                $newEmployee1->setCreatedAt($employee2->getCreatedAt());
                $newEmployee1->setModifiedAt($employee2->getModifiedAt());
                $em1->persist($newEmployee1);
                $em1->flush();
            };
        }
        $em1->flush();

        foreach ($employeeRepoDb1->findAll() as $employee1){
            $existEmployee2 = $employeeRepoDb2->findOneBy(['createdAt' => $employee1->getCreatedAt()]);
            if($existEmployee2){
                if ($existEmployee2->getModifiedAt() < $employee1->getModifiedAt()){
                    $existEmployee2->setName($employee1->getName());
                    $existEmployee2->setModifiedAt($employee1->getModifiedAt());
                    $em2->persist($existEmployee2);
                }
            }else{
                $newEmployee2 = new Employee2($employee1->getName());
                $newEmployee2->setCreatedAt($employee1->getCreatedAt());
                $newEmployee2->setModifiedAt($employee1->getModifiedAt());
                $em2->persist($newEmployee2);
                $em2->flush();
            };
        }
        $em2->flush();
    }
}
